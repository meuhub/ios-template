//
//  MainApp.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/23.
//

import SwiftUI
import Toaster

@main
struct MainApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(store).onAppear(perform: configureAppearance)
        }
    }
    
    func configureAppearance() {
        // 统一导航栏样式
        configNav()
        configToaster()
    }
    
    func configNav(){
        // 设置导航栏背景色
        UINavigationBar.appearance().barTintColor = UIColor(Color.white)
        // 去除导航栏下的线
        UINavigationBar.appearance().shadowImage = UIImage()
        // 设置导航栏上左右两边的 item 的颜色
//        UINavigationBar.appearance().tintColor = AppStyle.Colors.primaryText
        // 自定义返回按钮
        UINavigationBar.appearance().backIndicatorImage = UIImage(named: "icon_back")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named: "icon_back")
    }
    
    func configToaster(){
        let appearance = ToastView.appearance()
        appearance.bottomOffsetPortrait = 120
        appearance.font = .boldSystemFont(ofSize: 16)
        appearance.textInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        appearance.cornerRadius = 8
        ToastCenter.default.isSupportAccessibility = true
    }
    
}
