//
//  HomePage.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/26.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI
import SwiftyJSON

struct HomePage: View {
    @State var bannerData: JSON = []
    @State var selection = 0
    @State var searchText = ""
    
    var body: some View {
        VStack{
            HStack{
                BaseSearch(searchText: $searchText,onChange:{ value in
                    print(value)
                },onCommit: { value in
                    print(value)
                })
                Image("home_notice").resizable().frame(width:22,height: 22)
            }.padding(EdgeInsets(top: 12, leading: 16, bottom: 4, trailing: 16))
            ScrollView{
                BaseBanner(dataList: MockData.bannerData,name: "picturePath").padding(.horizontal).padding(.top,4)
                BaseCell(title: AnyView(Text("消息消息消息")),leading: AnyView(Image("home_notice_2").resizable().frame(width:18,height: 18)),trailing: AnyView(Image("home_notice_more").resizable().frame(width:22,height: 24))).padding(.horizontal).padding(.vertical,8)
                HomeTradeSwipeView().padding([.horizontal]).padding(.bottom,8)
                VStack{
                    Rectangle()
                        .fill(Color.background)
                        .frame(height: 12)
                    Image("home_activity_in").resizable().frame(height: 70).padding(EdgeInsets(.init(top: 8, leading: 16, bottom: 8, trailing: 16)))
                    Rectangle()
                        .fill(Color.background)
                        .frame(height: 12)
                }
                HomeActionView().padding(.vertical,8)
                HomeAddCoinView()
                BaseTabs(selection: $selection, list: ["涨幅榜","成交榜"],onChange: { index in
                    self.selection = index
                }).padding()
                
                let titleArr = ["名称","最新价","涨跌幅"]
                HStack{
                    ForEach(titleArr.indices){ index in
                        Text(titleArr[index]).font(.small).frame(maxWidth: .infinity, alignment: index == 2 ? .trailing : .leading)
                    }
                }.padding(.horizontal)
                
                TabView(selection: $selection) {
                    HomeMarketRowView(data: MockData.marketData).tag(0)
                    HomeMarketRowView(data: MockData.marketData).tag(1)
                }
                .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
            }
        }.font(.body).onAppear(perform: onLoad)
    }
    
    func onLoad(){
        NetWorkRequest(Api.banner(source: 1),successCallback: { result in
            for (index, item) in result.data.enumerated() {
                result.data[index]["picturePath"].string = AppConfig.ossUrl + "/" + result.data[index]["picturePath"].string!
            }
            self.bannerData = result.data
            print(self.bannerData)
        })
    }
}

struct HomePage_Previews: PreviewProvider {
    static var previews: some View {
        HomePage()
    }
}
