//
//  TradeSwipeView.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/31.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI
import SwiftyJSON

struct HomeTradeSwipeView: View {
    var data = MockData.TradingSwipeData

    @State private var selection = 0
    
    var body: some View {
        
        TabView(selection: $selection) {
            ForEach(0..<data.count){index in
                HStack{
                    ForEach(0..<data[index].count){index2 in
                        let item = data[index][index2]
                        VStack(alignment: .leading){
                            Text(item["ticker"] as! String)
                            Text(item["price"] as! String).foregroundColor((item["range"] as! NSNumber).doubleValue > 0 ? .green : (item["range"] as! NSNumber).doubleValue == 0 ? .gray : .red)
                            Text("\(((item["range"] as! NSNumber).doubleValue > 0 ? "+" : ""))\(String(format: "%.2f", item["range"] as! CVarArg))%").foregroundColor((item["range"] as! NSNumber).doubleValue > 0 ? .green : (item["range"] as! NSNumber).doubleValue == 0 ? .gray : .red)
                            Text("≈¥ \((String(format: "%.2f", item["cny"] as! CVarArg)))").font(.small).foregroundColor(.secondary)
                        }
                        if index2 != 2 {
                            Spacer()
                        }
                        
                    }
                }.tag(index)
            }
        }
        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
    }
}

struct HomeTradeSwipeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeTradeSwipeView()
    }
}

