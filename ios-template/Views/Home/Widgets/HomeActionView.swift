//
//  HomeActionView.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/31.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI

struct HomeActionView: View {
    var dataList = [
        [
            "icon":"home_add_coin",
            "text":"买币"
        ],
        [
            "icon":"home_invitation",
            "text":"返佣"
        ],
        [
            "icon":"home_poor",
            "text":"矿池"
        ],
        [
            "icon":"home_service",
            "text":"客服"
        ]
        
    ]
    var body: some View {
        LazyVGrid(columns: [GridItem(.flexible()),GridItem(.flexible()),GridItem(.flexible()),GridItem(.flexible())]){
            ForEach(dataList.indices){ index in
                VStack{
                    Image(dataList[index]["icon"]!).resizable().frame(width: 32, height: 32)
                    Text(dataList[index]["text"]!).font(.small)
                }
            }
        }
    }
}

struct HomeActionView_Previews: PreviewProvider {
    static var previews: some View {
        HomeActionView()
    }
}
