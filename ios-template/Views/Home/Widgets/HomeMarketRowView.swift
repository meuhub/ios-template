//
//  MarketRowView.swift
//  ios-template
//
//  Created by lvyii000 on 2021/9/1.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI
import SwiftyJSON

struct HomeMarketRowView: View {
    var data: [[String : Any]]
    
    var body: some View {
        VStack(spacing: 16){
            ForEach(data.indices){ index in
                let item = data[index]
                HStack{
                    VStack(alignment:.leading,spacing: 4){
                        Text(item["ticker"] as! String)
    //                    HStack{
    //                        Text("ETH")
    //                        Text("/ USDT").foregroundColor(.secondary).padding(.leading,-4)
    //                    }
                        Text("24H量 \(String(format: "%0.0f", item["vol"] as! CVarArg))").foregroundColor(.secondary)
                    }.frame(maxWidth:.infinity,alignment: Alignment.leading)
                    VStack(alignment:.leading,spacing: 4){
                        Text(item["price"]  as! String).foregroundColor((item["range"] as! NSNumber).doubleValue > 0 ? .green : (item["range"] as! NSNumber).doubleValue == 0 ? .regular : .red)
                        Text("≈¥ \(String(format: "%.2f", item["cny"] as! CVarArg))").foregroundColor(.regular)
                    }.frame(maxWidth:.infinity,alignment: Alignment.leading)
                    VStack(alignment:.trailing){
                        BaseButton(label:AnyView(Text("\(((item["range"] as! NSNumber).doubleValue > 0 ? "+" : ""))\(String(format: "%.2f", item["range"] as! CVarArg))%")),buttonType: (item["range"] as! NSNumber).doubleValue > 0 ? BaseButtonType.success : (item["range"] as! NSNumber).doubleValue == 0 ? BaseButtonType.info : BaseButtonType.danger,buttonSize: BaseButtonSize.mini,isRound: false,width: 100)
                    }.frame(maxWidth:.infinity,alignment: Alignment.trailing)
                }
            }
        }.padding()
    }
}

struct HomeMarketRowView_Previews: PreviewProvider {
    static var previews: some View {
        HomeMarketRowView(data: [])
    }
}
