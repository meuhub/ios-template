//
//  HomeAddCoinView.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/31.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI

struct HomeAddCoinView: View {
    var body: some View {
        ZStack{
            HStack{
                Image("home_add_coin_2").resizable().frame(width:46,height: 40)
                Spacer()
                VStack(alignment:.leading,spacing:8){
                    Text("快速充币").font(.extraLarge).fontWeight(.bold)
                    Text("支持BTC、USDT、ETH等").foregroundColor(.regular)
                }
                Spacer()
                Image("home_more").resizable().frame(width:22,height: 22)
            }
            .frame(height:73)
            .padding(.horizontal)
            .background(Color.init(UIColor.init(hexString: "#F7F9FF")))
            
            HStack{
                Image("home_bg_left").resizable().frame(width:109,height: 73)
                Spacer()
                Image("home_bg_right").resizable().frame(width:83,height: 73)
            }
            
        }
    }
}

struct HomeAddCoinView_Previews: PreviewProvider {
    static var previews: some View {
        HomeAddCoinView()
    }
}
