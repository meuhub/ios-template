//
//  MinePage.swift
//  ios-template
//
//  Created by lvyii000 on 2021/9/2.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI

struct MinePage: View {
    @State var selection: Int? = 0
    
    var body: some View {
        ScrollView{
            VStack{
                NavigationLink(
                    destination: Text("登录"),
                    label: {
                        Text("未登录").frame(maxWidth: .infinity,alignment: Alignment.leading).foregroundColor(.primary).font(.extraLarge).padding()
                    })
                Rectangle().fill(Color.background).frame(height:12)
                
                Group {
                    NavigationLink(destination: EmptyView(),tag: 1, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("安全设置")),clickable: true)
                    })
                    NavigationLink(destination: EmptyView(),tag: 2, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("身份验证")),clickable: true)
                    })
                    NavigationLink(destination: EmptyView(),tag: 3, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("邀请返佣")),clickable: true)
                    })
                    NavigationLink(destination: EmptyView(),tag: 4, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("等级费率")),clickable: true)
                    })
                    NavigationLink(destination: EmptyView(),tag: 5, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("我的消息")),clickable: true)
                    })
                    NavigationLink(destination: EmptyView(),tag: 6, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("我的工单")),clickable: true)
                    })
                    NavigationLink(destination: EmptyView(),tag: 6, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("我的广告")),clickable: true)
                    })
                }.padding(.horizontal).padding(.vertical,8).foregroundColor(.primary)
                
                Rectangle().fill(Color.background).frame(height:12)
                
                Group{
                    NavigationLink(destination: EmptyView(),tag: 7, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("在线客服")),clickable: true)
                    })
                    NavigationLink(destination: EmptyView(),tag: 8, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("黑名单")),clickable: true)
                    })
                    NavigationLink(destination: EmptyView(),tag: 9, selection: $selection, label: {
                        BaseCell(title:AnyView(Text("关于我们")),clickable: true)
                    })
                }.padding(.horizontal).padding(.vertical,8).foregroundColor(.primary)
                
            }
        }
    }
}

struct MinePage_Previews: PreviewProvider {
    static var previews: some View {
        MinePage()
    }
}
