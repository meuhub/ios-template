//
//  AppStylePage.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI

struct AppStylePage: View {
    var body: some View {
        List{
            Section(header: Text("字体大小").font(.small).padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))) {
                Text("辅助内容").font(.extraSmall)
                Text("次要内容").font(.small)
                Text("正文内容").font(.body)
                Text("小标题").font(.medium)
                Text("标题").font(.large)
                Text("大标题").font(.extraLarge)
             }
            Section(header: Text("字体颜色").font(.small).padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))) {
                Text("主要文本色").foregroundColor(.primary)
                Text("常规文本色").foregroundColor(.regular)
                Text("次要文本色").foregroundColor(.secondary)
                Text("占位文本色").foregroundColor(.placeholder)
             }
            Section(header: Text("主题颜色").font(.small).padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))) {
                HStack {
                    Text("主题色").foregroundColor(.primary)
                    Rectangle()
                        .fill(Color.primaryColor)
                        .frame(width: 120, height: 20)
                }
                HStack {
                    Text("成功色").foregroundColor(.success)
                    Rectangle()
                        .fill(Color.success)
                        .frame(width: 120, height: 20)
                }
                HStack {
                    Text("危险色").foregroundColor(.danger)
                    Rectangle()
                        .fill(Color.danger)
                        .frame(width: 120, height: 20)
                }
                HStack {
                    Text("警告色").foregroundColor(.warning)
                    Rectangle()
                        .fill(Color.warning)
                        .frame(width: 120, height: 20)
                }
                HStack {
                    Text("信息色").foregroundColor(.info)
                    Rectangle()
                        .fill(Color.info)
                        .frame(width: 120, height: 20)
                }
             }
        }.listStyle(SidebarListStyle())
    }
}

struct AppStylePage_Previews: PreviewProvider {
    static var previews: some View {
        AppStylePage()
    }
}
