//
//  AppStatePage.swift
//  ios-template
//
//  Created by lvyii000 on 2021/9/3.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI

struct AppStatePage: View {
    @EnvironmentObject var state: AppState
    
    var body: some View {
        BaseButton(label: AnyView(Text(state.isLogin ? "已登录" : "未登录"))).click {
            print(state.isLogin)
            state.isLogin = !state.isLogin
            state.send()
        }.padding()
    }
}

struct AppStatePage_Previews: PreviewProvider {
    static var previews: some View {
        AppStatePage()
    }
}
