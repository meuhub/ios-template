//
//  RequestPage.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI
import Toaster
import SwiftyJSON

struct RequestPage: View {
    @State var result = ""
    
    var body: some View {
        VStack{
            BaseButton(label: AnyView(Text("发送网络请求"))).click(perform: getData).padding()
            Text(result)
        }
    }
    
    func getData() -> Void{
        NetWorkRequest(Api.banner(source: 1),successCallback: { result in
            Toast(text: result.data.description).show()
        })
    }
}

struct RequestPage_Previews: PreviewProvider {
    static var previews: some View {
        RequestPage()
    }
}
