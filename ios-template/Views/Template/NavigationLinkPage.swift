//
//  NavigationLinkPage.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI

struct NavigationLinkPage: View {
    
    @State var selection: Int? = 0
    @State var isActive = false
    
    var body: some View {
        List{
            Section(header: Text("NavigationLink 添加点击事件无效 需控制跳转中添加业务").font(.small).padding(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))) {
                NavigationLink(destination: NavigationLinkDetailPage(), label:{
                    Text("自己跳转")
                })
                NavigationLink(destination: NavigationLinkDetailPage(), isActive: $isActive, label:{
                    Text("isActive值控制跳转").onTapGesture {
                        self.isActive = true
                    }
                })
                NavigationLink(destination: NavigationLinkDetailPage(), tag: 1, selection: $selection, label:{
                    Text("跳转tag1标识页面").onTapGesture {
                        self.selection = 1
                    }
                })
                NavigationLink(destination: NavigationLinkDetailPage(), tag: 2, selection: $selection, label:{
                    Text("跳转tag2标识页面").onTapGesture {
                        self.selection = 2
                    }
                })
                Link("跳转外链", destination: URL(string: "https://www.baidu.com")!)
             }
        }
    }
}

struct NavigationLinkDetailPage: View {
    
    @Environment(\.presentationMode) var presentation
    
    var body: some View {
        BaseButton(label: AnyView(Text("点击返回"))).click{
            presentation.wrappedValue.dismiss()
        }.padding()
    }
}

struct NavigationLinkPage_Previews: PreviewProvider {
    static var previews: some View {
        NavigationLinkPage()
    }
}
