//
//  CellPage.swift
//  ios-template
//
//  Created by 耳東陳 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI

struct CellPage: View {
    
    @State var passwordTip: String = ""
    
    var body: some View {
        ScrollView {
            BaseCell(
                title: AnyView(Text("title")), subTitle:AnyView(Text("subTitle")), leading: AnyView(Text("leading")),
                trailing: AnyView(Text("trailing").onTapGesture {
                    print("点击trailing")
                }),clickable: true
            ).onTapGesture {
                print("点击项")
            }.padding(.vertical,4)
            BaseCell(
                title: AnyView(Text("title")), subTitle:AnyView(Text("subTitle")),
                trailing: AnyView(Text("trailing")),clickable: true
            ).padding(.vertical,4)
            BaseCell(
                title: AnyView(Text("title")), subTitle:AnyView(Text("subTitle")), trailing: AnyView(Text("trailing")),clickable: false
            ).padding(.vertical,4)
            BaseCell(
                title: AnyView(Text("title")),clickable: true
            ).padding(.vertical,4)
            BaseCell(
                title: AnyView(TextField("请设置密码提示", text: self.$passwordTip)),leading: AnyView(Text("请输入密码")),clickable: false
            ).padding(.vertical,4)
            BaseCell(
                title: AnyView(TextField("请设置密码提示", text: self.$passwordTip)),leading: AnyView(Text("请输入密码")),trailing: AnyView(Button(action: {
                    
                            }, label: {
                                Text("trailing")
                            })
                ),clickable: false
            ).padding(.vertical,4)
        }.padding()
    }
}

struct CellPage_Previews: PreviewProvider {
    static var previews: some View {
        CellPage()
    }
}
