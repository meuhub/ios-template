//
//  ButtonPage.swift
//  ios-template
//
//  Created by 耳東陳 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI

struct ButtonPage: View {
    var body: some View {
        ScrollView{
            HStack {
                BaseButton(label: AnyView(Text("主要")),buttonType: BaseButtonType.primary,buttonSize: BaseButtonSize.mini).padding(.top)
                BaseButton(label: AnyView(Text("危险")),buttonType: BaseButtonType.danger,buttonSize: BaseButtonSize.mini).padding(.top)
                BaseButton(label: AnyView(Text("成功")),buttonType: BaseButtonType.success,buttonSize: BaseButtonSize.mini).padding(.top)
                BaseButton(label: AnyView(Text("警告")),buttonType: BaseButtonType.warning,buttonSize: BaseButtonSize.mini).padding(.top)
            }
            HStack {
                BaseButton(label: AnyView(Text("主要")),buttonType: BaseButtonType.primary,buttonSize: BaseButtonSize.mini,isPlain: true).padding(.top)
                BaseButton(label: AnyView(Text("危险")),buttonType: BaseButtonType.danger,buttonSize: BaseButtonSize.mini,isPlain: true).padding(.top)
                BaseButton(label: AnyView(Text("成功")),buttonType: BaseButtonType.success,buttonSize: BaseButtonSize.mini,isPlain: true).padding(.top)
                BaseButton(label: AnyView(Text("警告")),buttonType: BaseButtonType.warning,buttonSize: BaseButtonSize.mini,isPlain: true).padding(.top)
            }
            HStack {
                BaseButton(label: AnyView(Text("square")),buttonType: BaseButtonType.primary,buttonSize: BaseButtonSize.mini,isRound: false).padding(.top)
                BaseButton(label: AnyView(Text("circle")),buttonType: BaseButtonType.danger,buttonSize: BaseButtonSize.mini,isRound: true).padding(.top)
            }
            BaseButton(label: AnyView(Text("disabled")),buttonSize: BaseButtonSize.mini,disabled: true).padding(.top)
            BaseButton(label: AnyView(Text("mini")),buttonSize: BaseButtonSize.mini).padding(.top)
            BaseButton(label: AnyView(Text("medium")),buttonSize: BaseButtonSize.medium).padding(.top)
            BaseButton(label: AnyView(Text("large"))).padding()
        }
    }
}

struct ButtonPage_Previews: PreviewProvider {
    static var previews: some View {
        ButtonPage()
    }
}
