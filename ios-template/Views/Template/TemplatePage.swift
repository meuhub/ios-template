//
//  TemplatePage.swift
//  ios-template
//
//  Created by 耳東陳 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI

struct TemplatePage: View {
    @State var isEditing: Bool = false
    @State var searchText: String = ""
    
    var body: some View {
        List{
            NavigationLink(destination: AppStylePage(), label: {
                BaseCell(title: AnyView(Text("AppStyle示例")))
            })
            NavigationLink(destination: AppStatePage(), label: {
                BaseCell(title: AnyView(Text("AppState示例")))
            })
            NavigationLink(destination: CellPage(), label: {
                BaseCell(title: AnyView(Text("BaseCell组件示例")))
            })
            NavigationLink(destination: ButtonPage(), label: {
                BaseCell(title: AnyView(Text("BaseButton组件示例")))
            })
            NavigationLink(destination: PopupPage(), label: {
                BaseCell(title: AnyView(Text("Popup示例")))
            })
            NavigationLink(destination: NavigationLinkPage(), label: {
                BaseCell(title: AnyView(Text("NavigationLink路由示例")))
            })
            NavigationLink(destination: RealmPage(), label: {
                BaseCell(title: AnyView(Text("Realm数据库示例")))
            })
            NavigationLink(destination: RequestPage(), label: {
                BaseCell(title: AnyView(Text("Request网络请求示例")))
            })
            NavigationLink(destination: SocketPage(), label: {
                BaseCell(title: AnyView(Text("Socket连接示例")))
            })
        }.listStyle(SidebarListStyle())
    }
}

struct TemplatePage_Previews: PreviewProvider {
    static var previews: some View {
        TemplatePage()
    }
}
