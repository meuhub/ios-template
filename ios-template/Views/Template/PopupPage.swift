//
//  PopupPage.swift
//  ios-template
//
//  Created by lvyii000 on 2021/9/4.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI

struct PopupPage: View {
    @State private var showingAlert = false
    @State private var showingSheet = false
    @State private var showingActionSheet = false
    @State private var showButtonSheet = false
    
    var body: some View {
        Group{
            BaseButton(label: AnyView(Text("Show Alert")),buttonSize: .medium).click {
                self.showingAlert = true
            }.alert(isPresented: $showingAlert) {
                Alert(
                    title: Text("Title"),
                    message: Text("Message"),
                    primaryButton: .default(Text("Confirm")),
                    secondaryButton: .cancel()
                )
            }
            BaseButton(label: AnyView(Text("Show Sheet")),buttonSize: .medium).click {
                self.showingSheet = true
            }.sheet(isPresented: $showingSheet) {
                Text("Sheet").padding()
            }
            BaseButton(label: AnyView(Text("Show Action Sheet")),buttonSize: .medium).click {
                self.showingActionSheet = true
            }.actionSheet(isPresented: $showingActionSheet) {
                ActionSheet(title: Text("Title"), message: Text("Message"), buttons: [
                    .destructive(Text("Delete")),
                    .default(Text("Option 1")) { },
                    .default((Text("Option 2"))) { },
                    .cancel()
                ])
            }
            Menu("Show Menu") {
                Button("Button") {}
                Button("Button") {}
                Menu("Submenu") {
                    Button("Button") {}
                    Button("Button") {}
                    Button("Button") {}
                }
                Divider()
                Button("Button") {}
                Menu("Submenu") {
                    Button("Button") {}
                    Button("Button") {}
                    Button("Button") {}
                }
            }
        }
    }
}

struct PopupPage_Previews: PreviewProvider {
    static var previews: some View {
        PopupPage()
    }
}
