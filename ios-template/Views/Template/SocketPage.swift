//
//  SocketPage.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/23.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI
import SocketIO
import Toaster

struct SocketPage: View {
    var socket = socketManager.socket(forNamespace: "/kline.pusher?page=index")
    
    
    var body: some View {
        BaseButton(label: AnyView(Text("连接socket"))).click(perform: connectSocket).padding().onAppear(perform: connectSocket)
    }
    
    func connectSocket() -> Void {
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            socket.emit("req", ["req": "market.all.index_detail","source": "app"])
            socket.emit("sub", ["req": "market.all.index_detail","source": "app"])
        }

        socket.on("index_detail") {data, ack in
//            guard let cur = data[0] as? Double else { return }
            print("socket on")
            print(data)
        }
        socket.on(clientEvent: .error) {data, ack in
            Toast(text:data.description).show()
        }
        socket.connect()
    }
}

struct SocketPage_Previews: PreviewProvider {
    static var previews: some View {
        SocketPage()
    }
}
