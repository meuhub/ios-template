//
//  MarketPage.swift
//  ios-template
//
//  Created by lvyii000 on 2021/9/1.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI
import Toaster

struct MarketPage: View {
    @State var selection = 0
    var tabs = ["USDT","BTC","ETH","XRP"]
    
    var body: some View {
        VStack{
            BaseTabs(selection: $selection, list: tabs,onChange: { index in
                self.selection = index
            }).padding()
            
            let titleArr = ["名称","最新价","涨跌幅"]
            HStack{
                ForEach(titleArr.indices){ index in
                    Text(titleArr[index]).font(.small).frame(maxWidth: .infinity, alignment: index == 2 ? .trailing : .leading)
                }
            }.padding(.horizontal)
            
            ScrollView{
                TabView(selection: $selection) {
                    ForEach(0..<tabs.count){ index in
                        HomeMarketRowView(data: MockData.marketData).tag(index)
                    }
                }
                .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
            }
        }
    }
}

struct MarketPage_Previews: PreviewProvider {
    static var previews: some View {
        MarketPage()
    }
}
