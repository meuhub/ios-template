//
//  MockData.swift
//  ios-template
//
//  Created by lvyii000 on 2021/9/1.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import Foundation
import SwiftyJSON

struct MockData {
    static var bannerData: JSON = [
        ["picturePath": "https://bandex-dev.oss-cn-shenzhen.aliyuncs.com/1619665403045.jpg"],
        ["picturePath": "https://bandex-dev.oss-cn-shenzhen.aliyuncs.com/1619665403045.jpg"],
        ["picturePath": "https://bandex-dev.oss-cn-shenzhen.aliyuncs.com/1619665403045.jpg"]]

    static var TradingSwipeData = [[["id":1630377575291,"ticker":"BTC_USDT","price":"46943.78","low":"46691.54","high":"48703.37","vol":167.427164,"amount":50459041.3,"cny":301379.06,"range":-3.24],["id":1630377575291,"ticker":"ETH_USDT","price":"3221.22","low":"7.10","high":"3345.50","vol":4348.098,"amount":89919666.7,"cny":20680.23,"range":0.42],["id":1630377575291,"ticker":"EOS_USDT","price":"4.8035","low":"4.1000","high":"7.9000","vol":6331230.3744,"amount":195245457.96,"cny":30.83847,"range":-3.97]],[["id":1630377575291,"ticker":"EOS_ETH","price":"0.00152261","low":"0.00152261","high":"0.00152261","vol":0,"amount":0,"cny":31.48792,"range":0],["id":1630377575291,"ticker":"EOS_BTC","price":"0.00010000","low":"0.00010000","high":"0.00010000","vol":0,"amount":0,"cny":30.1379,"range":0],["id":1630377575291,"ticker":"ETH_BTC","price":"0.017300","low":"0.017300","high":"0.017300","vol":0,"amount":0,"cny":5213.85,"range":0]]]

    static var marketData =  [["id":"null","ticker":"ETH_USDT","price":"3419.84","low":"3215.10","high":"3469.37","vol":4445.7849,"amount":97608852.41,"favorite":false,"cny":21955.37,"fixPrice":"null","range":6.18,"rate":"null","symbol":"null","icon":"1593672050879.png"],["id":"null","ticker":"EOS_USDT","price":"4.9836","low":"4.8038","high":"5.0989","vol":6054.4332,"amount":193709.83,"favorite":false,"cny":31.99471,"fixPrice":"null","range":3.19,"rate":"null","symbol":"null","icon":"1593671997606.png"],["id":"null","ticker":"BTC_USDT","price":"46943.78","low":"46943.78","high":"46943.78","vol":0,"amount":0,"favorite":true,"cny":301379.06,"fixPrice":"null","range":0,"rate":"null","symbol":"null","icon":"1593671926525.png"],["id":"null","ticker":"XRP_USDT","price":"1.11410","low":"1.11410","high":"1.11410","vol":0,"amount":0,"favorite":false,"cny":7.15,"fixPrice":"null","range":0,"rate":"null","symbol":"null","icon":"1593672121178.png"],["id":"null","ticker":"LTC_USDT","price":"167.43","low":"167.43","high":"167.43","vol":0,"amount":0,"favorite":false,"cny":1074.9,"fixPrice":"null","range":0,"rate":"null","symbol":"null","icon":"1593672214301.png"],["id":"null","ticker":"BCH_USDT","price":"628.44","low":"628.44","high":"628.44","vol":0,"amount":0,"favorite":true,"cny":4034.58,"fixPrice":"null","range":0,"rate":"null","symbol":"null","icon":"1593672418814.png"],["id":"null","ticker":"BSV_USDT","price":"161.9720","low":"161.9720","high":"161.9720","vol":0,"amount":0,"favorite":false,"cny":1039.86,"fixPrice":"null","range":0,"rate":"null","symbol":"null","icon":"1593685772120.png"],["id":"null","ticker":"TRX_USDT","price":"0.085600","low":"0.085600","high":"0.085600","vol":0,"amount":0,"favorite":false,"cny":0.54,"fixPrice":"null","range":0,"rate":"null","symbol":"null","icon":"1593672605881.png"],["id":"null","ticker":"XMR_USDT","price":"281.99","low":"281.99","high":"281.99","vol":0,"amount":0,"favorite":false,"cny":1810.37,"fixPrice":"null","range":0,"rate":"null","symbol":"null","icon":"1593672685528.png"],["id":"null","ticker":"DASH_USDT","price":"214.82","low":"214.82","high":"214.82","vol":0,"amount":0,"favorite":false,"cny":1379.14,"fixPrice":"null","range":0,"rate":"null","symbol":"null","icon":"1593672859270.png"]]

}
