
//
//  AppStyle.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class AppStyle {
        
    // 横向间距
    static var spacingHorizontal = 16
    // 纵向间距
    static var spacingVertical = 12
    
    // 圆角
    static var radiusButton = 5
    static var radiusBase = 8
    static var radiusRound = 999

    // 内容项高度
    static var listItemHeight = 48

}

extension Font {
    // Large Title：34
    // Title：28
    // Title2：22
    // Title3：20
    // Title3：20
    // HeadLine：17 加粗
    // Body：17
    // Callout：16
    // SubHead：15
    // Footnote：13
    // Cation1：12
    // Cation2：11
    
    // 辅助内容
    public static var extraSmall: Font {
        return Font.custom("OpenSans-Regular", size: 12)
    }
    
    // 次要内容
    public static var small: Font {
        return Font.custom("OpenSans-Regular", size: 14)
    }
    
    // 正文内容
    public static var body: Font {
        return Font.custom("OpenSans-Regular", size: 16)
    }
    
    // 小标题
    public static var medium: Font {
        return Font.custom("OpenSans-Regular", size: 18)
    }
    
    // 标题
    public static var large: Font {
        return Font.custom("OpenSans-Regular", size: 20)
    }
    
    // 大标题
    public static var extraLarge: Font {
        return Font.custom("OpenSans-Regular", size: 22)
    }
}

extension Color {
//    public static let clear: Color
//
//    public static let black: Color
//
//    public static let white: Color
//
//    public static let gray: Color
//
//    public static let red: Color
//
//    public static let green: Color
//
//    public static let blue: Color
//
//    public static let orange: Color
//
//    public static let yellow: Color
//
//    public static let pink: Color
//
//    public static let purple: Color
//
//    public static let primary: Color
//
//    public static let secondary: Color
    
    // 主题色
    public static var primaryColor: Color {
        return Color.init(UIColor.init(hexString: "#3F81FF"))
    }
    // 成功色
    public static var success: Color {
        return Color.init(UIColor.init(hexString: "#67C23A"))
    }
    // 危险色
    public static var danger: Color {
        return Color.init(UIColor.init(hexString: "#F56C6C"))
    }
    // 警告色
    public static var warning: Color {
        return Color.init(UIColor.init(hexString: "#E6A23C"))
    }
    // 信息色
    public static var info: Color {
        return Color.init(UIColor.init(hexString: "#909399"))
    }
    
    
    // 主要文本色
    public static var primary: Color {
        return Color.init(UIColor.init(hexString: "#303133"))
    }
    // 常规文本色
    public static var regular: Color {
        return Color.init(UIColor.init(hexString: "#606266"))
    }
    // 次要文本色
    public static var secondary: Color {
        return Color.init(UIColor.init(hexString: "#909399"))
    }
    // 占位文本色
    public static var placeholder: Color {
        return Color.init(UIColor.init(hexString: "#C0C4CC"))
    }
    
    // 页面背景色
    public static var background: Color {
        return Color.init(UIColor.init(hexString: "#F8F8F8"))
    }
    // 灰色
//    public static var gray: Color {
//        return Color.init(UIColor.init(hexString: "#EEEEEE"))
//    }
}

extension UIColor {
     
    // Hex String -> UIColor
    convenience init(hexString: String) {
        let hexString = hexString.trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
         
        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
         
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
         
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
         
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
         
        self.init(red: red, green: green, blue: blue, alpha: 1)
    }
}
