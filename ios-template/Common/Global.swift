
//
//  Global.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import Foundation
import UIKit
import SocketIO

let ScreenWidth = UIScreen.main.bounds.size.width
let ScreenHeight = UIScreen.main.bounds.size.height

let NavBarHeight = isIphoneX ? 88 : 64
let TabBarHeight = isIphoneX ? 49 + 34 : 49

var isIphoneX: Bool {
    return UI_USER_INTERFACE_IDIOM() == .phone
        && (max(UIScreen.main.bounds.height, UIScreen.main.bounds.width) == 812
        || max(UIScreen.main.bounds.height, UIScreen.main.bounds.width) == 896)
}

let socketManager = SocketManager(socketURL: URL(string: AppConfig.baseSocket)!, config: [.log(false), .compress])

enum UserDefaultKey: String {
    case login = "LoginKey"
}
