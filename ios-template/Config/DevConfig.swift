//
//  DevConfig.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/27.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import Foundation

struct DevConfig {
    var baseUrl = "http://dev.entry.bandex.lvyii.com"
    var baseSocket = "ws://dev.ws.klinepusher.bandex.lvyii.com"
    var ossUrl = "https://bandex-dev.oss-cn-shenzhen.aliyuncs.com"
}
