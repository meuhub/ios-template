//
//  ProdConfig.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/27.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import Foundation

struct ProdConfig {
    var baseUrl = "https://app.cowbitex.com"
    var baseSocket = "wss://wss.pusher.cowbitex.com"
    var ossUrl = "https://cbhk.oss-cn-hongkong.aliyuncs.com"
}
