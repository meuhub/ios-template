//
//  BaseCell.swift
//  ios-template
//
//  Created by 耳東陳 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI

struct BaseCell: View {
    
    var title: AnyView?
    var subTitle: AnyView?
    var leading: AnyView?
    var trailing: AnyView?
    var clickable: Bool? = false

    var body: some View {
        HStack{
            if nil != leading {
                leading
            }
            if  nil != subTitle {
                VStack(alignment: .leading){
                    title
                    subTitle.foregroundColor(.secondary).font(.subheadline).padding(EdgeInsets(top: 1, leading: 0, bottom: 0, trailing: 0))
                }
            } else {
                title
            }
            Spacer()
            HStack{
                if nil != trailing {
                    trailing
                }
                if clickable! {
                    Image(systemName: "chevron.right").foregroundColor(.placeholder).font(.system(size: 12, weight: Font.Weight.bold))
                }
                
            }
        }.contentShape(Rectangle())
    }
}

struct BaseCell_Previews: PreviewProvider {
    
    static var previews: some View {
        ScrollView {
            BaseCell(
                title: AnyView(Text("title")), subTitle:AnyView(Text("subTitle")), leading: AnyView(Text("leading")),
                trailing: AnyView(Text("trailing").onTapGesture {
                    print("点击trailing")
                })
            ).onTapGesture {
                print("点击项")
            }
            BaseCell(
                title: AnyView(Text("title")), subTitle:AnyView(Text("subTitle")),
                trailing: AnyView(Text("trailing"))
            )
            BaseCell(
                title: AnyView(Text("title")), subTitle:AnyView(Text("subTitle")), trailing: AnyView(Text("trailing")),clickable: false
            )
            BaseCell(
                title: AnyView(Text("title"))
            )
        }.padding()
    }
}

