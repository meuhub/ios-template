//
//  BaseTabs.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/31.
//  Copyright © 2021 耳東陈. All rights reserved.
//

import SwiftUI
import SwiftyJSON

struct BaseTabs: View {
    @Binding var selection: Int
    var list: [String]
    var name: String?
    var onChange: ((Int) -> Void)?
    
    var body: some View {
        ScrollView(.horizontal) {
            ScrollViewReader { scroll in
                HStack(spacing: 16){
                    ForEach(list.indices) { index in
                        VStack{
                            Text("\(self.list[index])").font(selection == index ? .medium : .body)
                            Rectangle()
                                .fill(Color.primaryColor).opacity(selection == index ? 1 : 0)
                                .frame(width:30, height: 2)
                                .cornerRadius(30)
                            
                        }.id(index).onTapGesture {
                            self.selection = index
                            scroll.scrollTo(index, anchor: .top)
                            if self.onChange != nil {
                                self.onChange!(index)
                            }
                        }
                    }
                }
            }
        }
    }
}

struct BaseTabs_Previews: PreviewProvider {
    static var previews: some View {
        BaseTabs(selection: .constant(0), list: ["Tab1","Tab2"])
    }
}
