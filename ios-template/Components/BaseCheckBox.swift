//
//  BaseCheckBox.swift
//  ios-template
//
//  Created by 耳東陳 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI

struct BaseCheckBox: View {
    
    @Binding var isChecked: Bool
    var title: String
    
    var body: some View {
        Button(action: { self.isChecked.toggle() }) {
            HStack {
                Image(systemName: isChecked ? "checkmark.square": "square")
                Text(title)
            }
            .foregroundColor(isChecked ? .primary : .secondary)
        }
    }
}

struct CheckBox_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            BaseCheckBox(isChecked: .constant(true), title: "Test checkbox")
            BaseCheckBox(isChecked: .constant(false), title: "Test checkbox")
        }
    }
}
