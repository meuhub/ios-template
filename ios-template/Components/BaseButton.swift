//
//  BaseButton.swift
//  ios-template
//
//  Created by 耳東陳 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI
import UIKit

enum BaseButtonType {
    case primary
    case success
    case danger
    case warning
    case info
}

enum BaseButtonSize {
    case mini
    case medium
    case large
}

struct BaseButton: View {
    var label: AnyView?
    var buttonType: BaseButtonType? = BaseButtonType.primary
    var buttonSize: BaseButtonSize? = BaseButtonSize.large
    var isRound: Bool? = true
    var isPlain: Bool? =  false
    var disabled: Bool? = false
    var width: CGFloat! = 0
    var height: CGFloat! = 0
    
    var action: (() -> Void)? = {}

    var body: some View {
        if !isPlain! {
            Button(action: action!, label: {
                label
                    .padding(EdgeInsets(top: buttonSize == BaseButtonSize.mini ? 8 : 15, leading: 15, bottom: buttonSize == BaseButtonSize.mini ? 8 : 15, trailing: 15))
                    .frame(minWidth: 0, maxWidth: width != 0 ? width : buttonSize == BaseButtonSize.mini ? .none : getFrame(size: buttonSize!))
                    .foregroundColor(Color.white)
                    .background(getBackgroundColor(type: buttonType!)).opacity(disabled ?? false ? 0.6 : 1)
                    .cornerRadius(isRound ?? true ? 30 : 5)
            }).disabled(disabled ?? false)
        } else {
            Button(action: action!, label: {
                label
                    .padding(EdgeInsets(top: buttonSize == BaseButtonSize.mini ? 8 : 15, leading: 15, bottom: buttonSize == BaseButtonSize.mini ? 8 : 15, trailing: 15))
                    .frame(minWidth: 0, maxWidth: width != 0 ? width : buttonSize == BaseButtonSize.mini ? .none : getFrame(size: buttonSize!))
                    .foregroundColor(getBackgroundColor(type: buttonType!))
                    .background(RoundedRectangle(cornerRadius: isRound ?? true ? 30 : 5)
                                    .stroke(getBackgroundColor(type: buttonType!),lineWidth: 1)).opacity(disabled ?? false ? 0.6 : 1)
                    .cornerRadius(isRound ?? true ? 30 : 5)
            }).disabled(disabled ?? false)
        }
        
    }
    
    func getFrame(size: BaseButtonSize) -> CGFloat {
        switch size {
            case BaseButtonSize.large:
                return .infinity
            case BaseButtonSize.medium:
                return ScreenWidth / 2
            default:
                return 0
        }
    }
    
    func getBackgroundColor(type: BaseButtonType) -> Color {
        switch (type) {
            case BaseButtonType.success:
                return Color.success
            case BaseButtonType.danger:
                return Color.danger
            case BaseButtonType.warning:
                return Color.warning
            case BaseButtonType.info:
                return Color.info
        default: return Color.primaryColor
        }
        
        
    }
}

extension BaseButton {

    func click(perform action: @escaping () -> Void ) -> Self {
        var this = self
        this.action = action
        return this
    }
}

struct BaseButton_Previews: PreviewProvider {
    static var previews: some View {
        ScrollView{
            HStack {
                BaseButton(label: AnyView(Text("主要")),buttonType: BaseButtonType.primary,buttonSize: BaseButtonSize.mini).padding(.top)
                BaseButton(label: AnyView(Text("危险")),buttonType: BaseButtonType.danger,buttonSize: BaseButtonSize.mini).padding(.top)
                BaseButton(label: AnyView(Text("成功")),buttonType: BaseButtonType.success,buttonSize: BaseButtonSize.mini).padding(.top)
                BaseButton(label: AnyView(Text("警告")),buttonType: BaseButtonType.warning,buttonSize: BaseButtonSize.mini).padding(.top)
            }
            HStack {
                BaseButton(label: AnyView(Text("主要")),buttonType: BaseButtonType.primary,buttonSize: BaseButtonSize.mini,isPlain: true).padding(.top)
                BaseButton(label: AnyView(Text("危险")),buttonType: BaseButtonType.danger,buttonSize: BaseButtonSize.mini,isPlain: true).padding(.top)
                BaseButton(label: AnyView(Text("成功")),buttonType: BaseButtonType.success,buttonSize: BaseButtonSize.mini,isPlain: true).padding(.top)
                BaseButton(label: AnyView(Text("警告")),buttonType: BaseButtonType.warning,buttonSize: BaseButtonSize.mini,isPlain: true).padding(.top)
            }
            HStack {
                BaseButton(label: AnyView(Text("square")),buttonType: BaseButtonType.primary,buttonSize: BaseButtonSize.mini,isRound: false).padding(.top)
                BaseButton(label: AnyView(Text("circle")),buttonType: BaseButtonType.danger,buttonSize: BaseButtonSize.mini,isRound: true).padding(.top)
            }
            BaseButton(label: AnyView(Text("disabled")),buttonSize: BaseButtonSize.mini,disabled: true).padding(.top)
            BaseButton(label: AnyView(Text("mini")),buttonSize: BaseButtonSize.mini).padding(.top)
            BaseButton(label: AnyView(Text("medium")),buttonSize: BaseButtonSize.medium).padding(.top)
            BaseButton(label: AnyView(Text("large"))).padding()
        }
    }
}
