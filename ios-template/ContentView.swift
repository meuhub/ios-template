//
//  ContentView.swift
//  ios-template
//
//  Created by 耳東陳 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var selection: Int = 0
    @EnvironmentObject var state: AppState
    
    var body: some View {
        NavigationView{
            TabView(selection: $selection) {
                HomePage()
                    .tabItem { Item(type: .home, selection: selection) }
                    .tag(ItemType.home.rawValue)
                MarketPage()
                    .tabItem { Item(type: .market, selection: selection) }
                    .tag(ItemType.market.rawValue)
                MinePage()
                    .tabItem { Item(type: .mine, selection: selection) }
                    .tag(ItemType.mine.rawValue)
                TemplatePage()
                    .tabItem { Item(type: .template, selection: selection) }
                    .tag(ItemType.template.rawValue)
            }
            .navigationBarHidden(selection == 0)
            .navigationBarTitle(itemType.title, displayMode: .inline)
            .navigationBarItems(trailing: itemType.navigationBarTrailingItems(selection: selection))
        }
    }
    
    enum ItemType: Int {
        case home
        case market
        case mine
        case template
        
        var image: Image {
            switch self {
            case .home:  return Image("tab_home_normal")
            case .market:  return Image("tab_home_normal")
            case .mine:  return Image("tab_mine_normal")
            case .template:  return Image("tab_mine_normal")
            }
        }
        
        var selectedImage: Image {
            switch self {
            case .home:     return Image("tab_home_select")
            case .market:     return Image("tab_home_select")
            case .mine:  return Image("tab_mine_select")
            case .template:  return Image("tab_mine_select")
            }
        }
        
        var title: String {
            switch self {
            case .home:     return "首页"
            case .market:     return "行情"
            case .mine:  return "我的"
            case .template:  return "组件"
            }
        }
        
        
        func isNavigationBarHidden(selection: Int) -> Bool {
            return true
        }
        
        func navigationBarTrailingItems(selection: Int) -> AnyView {
            switch ItemType(rawValue: selection)! {
            case .home:
                return AnyView(EmptyView())
            case .market:
                return AnyView(HStack{
                    NavigationLink(
                        destination: Text("自选排序"),
                        label: {
                            Image("market_edit").resizable().frame(width:18,height: 18).foregroundColor(.secondary)
                        })
                    NavigationLink(
                        destination: Text("搜索"),
                        label: {
                            Image("search").resizable().frame(width:18,height: 18).foregroundColor(.secondary)
                    })
                })
            case .mine:
                return AnyView(EmptyView())
            case .template:
                return AnyView(EmptyView())

            }
        }
    }
    
    struct Item: View {
        let type: ItemType
        let selection: Int
        
        var body: some View {
            VStack {
                if type.rawValue == selection {
                    type.selectedImage
                } else {
                    type.image
                }
                
                Text(type.title).foregroundColor(.black)
            }
        }
    }
    
    private var itemType: ItemType { ItemType(rawValue: selection)! }
}

struct RootTabView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
