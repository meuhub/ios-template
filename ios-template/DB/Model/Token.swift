//
//  TokenModel.swift
//  ios-template
//
//  Created by 耳東陳 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import UIKit
import RealmSwift

class Token: Object {
//    @objc dynamic var id = 0
    @objc dynamic var chain_symbol = 0
    @objc dynamic var wallet_id = 0
    @objc dynamic var address = ""
    @objc dynamic var name = ""
    @objc dynamic var symbol = ""
    @objc dynamic var decimals = 2
    @objc dynamic var contract = ""
    @objc dynamic var path = ""
    @objc dynamic var balance = ""

//    override static func primaryKey() -> String? {
//        return "id"
//    }

}


