//
//  API.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import Moya

let ApiProvider = MoyaProvider<Api>()

//MARK: API定义
enum Api {
    case banner(source: Int)
    case goodsDetail(id: String)
    
}

extension Api: TargetType {
    
    var baseURL: URL { return URL(string: AppConfig.baseUrl)! }
    var path: String {
        switch self {
        case .banner:
            return "/cms/banner"
        case .goodsDetail:
            return "/shop/goods/detail"
        
        }
    }
    var method: Moya.Method {
        switch self {
        case .banner:
            return .get
        default:
            return .get
        }
    }
    var task: Task {
        var parmeters: [String : Any] = [:]
        switch self {
        case .banner(let source):
            parmeters["source"] = source
            
        default: break
        }
        parmeters["langCode"] = store.langCode
        return .requestParameters(parameters: parmeters, encoding: URLEncoding.default)
    }
    
    var sampleData: Data { return "".data(using: String.Encoding.utf8)! }
    var headers: [String: String]? {
        return ["Content-type": "application/x-www-form-urlencoded"]
    }
}
