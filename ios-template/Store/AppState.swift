//
//  AppState.swift
//  ios-template
//
//  Created by 耳東陳 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

class AppState: ObservableObject {
    var objectWillChange = PassthroughSubject<AppState, Never>()
    
    var userStore = UserStore()
    
    var isLogin = false
    var token = "token"
    var langCode = "zh_CN"
    var isShowTab = false
    
    func send() {
        objectWillChange.send(self)
    }
}

var store = AppState()
