//
//  UserStore.swift
//  ios-template
//
//  Created by lvyii000 on 2021/8/23.
//  Copyright © 2021 耳東陳. All rights reserved.
//

import Foundation

class UserStore {
    var isLogin = UserDefaults.standard.bool(forKey: UserDefaultKey.login.rawValue)
    
    func dispatch(action: String,data: [String:Any]? = nil) -> Void {
        switch action {
        case "login":
            UserDefaults.standard.set(true, forKey: UserDefaultKey.login.rawValue)
            self.isLogin = true
        case "logout":
            UserDefaults.standard.set(false, forKey: UserDefaultKey.login.rawValue)
            self.isLogin = false
        default:
            break
        }
        
        DispatchQueue.main.async {
            store.send()
        }
        
    }
}
